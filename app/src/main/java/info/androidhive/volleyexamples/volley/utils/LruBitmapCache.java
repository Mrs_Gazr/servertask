package info.androidhive.volleyexamples.volley.utils;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by HendRaafat on 25/03/2017.
 */

public class LruBitmapCache extends LruCache<String, Bitmap> implements
        public static int getDefaultLruCacheSize() {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 8;

            return cacheSize;
        }
// hary dfdfdkfndfjndflkjn
        public LruBitmapCache() {
            this(getDefaultLruCacheSize());
        }

        public LruBitmapCache(int sizeInKiloBytes) {
            super(sizeInKiloBytes);
        }

        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getRowBytes() * value.getHeight() / 1024;
        }@Override
public Bitmap getBitmap(String url) {
    return get(url);
}

        @Override
        public void putBitmap(String url, Bitmap bitmap) {
            put(url, bitmap);
        }
}